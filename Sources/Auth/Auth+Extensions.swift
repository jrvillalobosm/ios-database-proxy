//
//  Auth+Extensions.swift
//
//  Created by Jorge Villalobos on 18/10/18.
//  Copyright © 2018 Jorge Villalobos. All rights reserved.
//

import FirebaseAuth
import Foundation
import RxSwift

extension Reactive where Base: Auth {
    public func sendPasswordReset(withEmail email: String,
        actionCodeSettings: ActionCodeSettings? = nil
    ) -> Observable<Void> {
        return Observable.create { observer in
            let completion: SendPasswordResetCallback = { error in
                guard let error = error else {
                    observer.onNext(())
                    observer.onCompleted()
                    return
                }
                observer.onError(AuthenticationErrors(error as NSError))
            }
            if let actionCodeSettings = actionCodeSettings {
                self.base
                    .sendPasswordReset(withEmail: email, actionCodeSettings: actionCodeSettings, completion: completion)
            } else {
                self.base.sendPasswordReset(withEmail: email, completion: completion)
            }
            return Disposables.create()
        }
    }

    public func fetchProviders(forEmail email: String) -> Observable<[String]> {
        return Observable.create { observer in
            self.base.fetchProviders(forEmail: email) { providers, error in
                guard let error = error else {
                    observer.onNext(providers ?? [])
                    observer.onCompleted()
                    return
                }
                observer.onError(AuthenticationErrors(error as NSError))
            }
            return Disposables.create()
        }
    }

    public func createUser(withEmail email: String, password: String) -> Observable<AuthDataResult> {
        return Observable.create { observer in
            self.base.createUser(withEmail: email, password: password) { auth, error in
                guard let error = error else {
                    if let auth = auth {
                        observer.onNext(auth)
                    }
                    observer.onCompleted()
                    return
                }
                observer.onError(AuthenticationErrors(error as NSError))
            }
            return Disposables.create()
        }
    }

    public func signOut() -> Observable<Bool> {
        return Observable.create { observer in
            do {
                if self.base.currentUser != nil {
                    try self.base.signOut()
                }
                observer.onNext(true)
                observer.onCompleted()
            } catch let error {
                observer.onError(AuthenticationErrors(error as NSError))
            }
            return Disposables.create()
        }
    }

    public func signInAnonymously() -> Observable<AuthDataResult> {
        return Observable.create { observer in
            self.base.signInAnonymously { auth, error in
                guard let error = error else {
                    if let auth = auth {
                        observer.onNext(auth)
                    }
                    observer.onCompleted()
                    return
                }
                observer.onError(AuthenticationErrors(error as NSError))
            }
            return Disposables.create()
        }
    }

    public func signIn(withCustomToken token: String) -> Observable<AuthDataResult> {
        return Observable.create { observer in
            self.base.signIn(withCustomToken: token) { auth, error in
                guard let error = error else {
                    if let auth = auth {
                        observer.onNext(auth)
                    }
                    observer.onCompleted()
                    return
                }
                observer.onError(AuthenticationErrors(error as NSError))
            }
            return Disposables.create()
        }
    }

    public func signIn(withEmail email: String, password: String) -> Observable<AuthDataResult> {
        return Observable.create { observer in
            self.base.signIn(withEmail: email, password: password) { auth, error in
                guard let error = error else {
                    if let auth = auth {
                        observer.onNext(auth)
                    }
                    observer.onCompleted()
                    return
                }
                observer.onError(AuthenticationErrors(error as NSError))
            }
            return Disposables.create()
        }
    }

    public func signIn(withEmail email: String, link: String) -> Observable<AuthDataResult> {
        return Observable.create { observer in
            self.base.signIn(withEmail: email, link: link) { auth, error in
                guard let error = error else {
                    if let auth = auth {
                        observer.onNext(auth)
                    }
                    observer.onCompleted()
                    return
                }
                observer.onError(AuthenticationErrors(error as NSError))
            }
            return Disposables.create()
        }
    }

    public func signInAndRetrieveData(with credential: AuthenticationCredential) -> Observable<AuthDataResult> {
        return Observable.create { observer in
            self.base.signInAndRetrieveData(with: credential) { auth, error in
                guard let error = error else {
                    if let auth = auth {
                        observer.onNext(auth)
                    }
                    observer.onCompleted()
                    return
                }
                observer.onError(AuthenticationErrors(error as NSError))
            }
            return Disposables.create()
        }
    }

    public func addStateDidChangeListener() -> Observable<User?> {
        return Observable.create { observer in
            let handle = self.base.addStateDidChangeListener { _, user in
                observer.onNext(user)
            }
            return Disposables.create {
                self.base.removeStateDidChangeListener(handle)
            }
        }
    }
}
