//
//  UserLayer.swift
//
//  Created by Jorge Villalobos on 07/11/18.
//  Copyright © 2018 Jorge Villalobos. All rights reserved.
//

import CommonsAssets
import FacebookCore
import FacebookLogin
import FirebaseAuth
import Foundation
import RxSwift

public enum StatusAuth {
    case anonymous
    case logged
    case notLogged
}

public class GenericUser {
    public init() { }

    public var currentUser: AuthUser? {
        return Auth.auth().currentUser
    }

    public var uid: String {
        return self.currentUser!.uid
    }

    public var statusAuth: StatusAuth {
        guard let user = self.currentUser else {
            return .notLogged
        }
        return (user.isAnonymous) ? .anonymous : .logged
    }

    internal var user: Observable<AuthUser> {
        if let user = self.currentUser {
            return Observable.just(user)
        }
        return Observable.error(AuthenticationErrors.undefinedUser)
    }

    public func getIDToken() -> Observable<String> {
        return self.user.flatMap { user -> Observable<String> in
            user.rx.getIDToken()
        }
    }

    public func reauthenticateAndRetrieveData(with credential: AuthenticationCredential) -> Observable<AuthUser> {
        return self.user.flatMap { user -> Observable<AuthUser> in
            user.rx.reauthenticateAndRetrieveData(with: credential)
                .map { result -> User in
                    return result.user
            }
        }
    }

    public func linkAndRetrieveData(with credential: AuthenticationCredential) -> Observable<AuthUser> {
        return self.user.flatMap { user -> Observable<AuthUser> in
            user.rx.linkAndRetrieveData(with: credential)
                .map { result -> User in
                    return result.user
            }
        }
    }

    public func sendEmailVerification() -> Observable<Void> {
        return self.user.flatMap { user -> Observable<Void> in
            user.rx.sendEmailVerification()
        }
    }
}

public class AuthenticationUser: GenericUser {
    public override init() {
        super.init()
    }

    public var currentEmail: String {
        guard let user = self.currentUser else {
            return "Undefined"
        }
        return user.currentEmail
    }

    public func updatePassword(to password: String) -> Observable<Void> {
        return self.user.flatMap { user -> Observable<Void> in
            user.rx.updatePassword(to: password)
        }
    }

    public func signOut() -> Observable<Bool> {
        return Auth.auth().rx.signOut()
    }

    public func delete() -> Observable<Bool> {
        if let currentUser = self.currentUser {
            return currentUser.rx.delete()
                .map({ true })
                .catchError { [weak self] _ in
                    guard let weakSelf = self else { return Observable.empty() }
                    return weakSelf.signOut()
            }
        } else {
            return Observable.just(false)
        }
    }
}

public class FacebookUser: GenericUser {
    private var loginManager: LoginManager?

    public var readPermissions: [ReadPermission] = [.publicProfile, .email]
    public var loginBehavior: LoginBehavior = .native
    public var graphPath: String = "me"
    public var parameters: Json = ["fields": "id, name, picture.type(large), email"]

    public override init() {
        super.init()
    }

    public func logOut() -> Observable<Void> {
        guard let loginManager = self.loginManager else {
            let loginManager = LoginManager()
            loginManager.loginBehavior = self.loginBehavior
            return loginManager.rx.logOut()
        }
        return loginManager.rx.logOut()
    }

    public func authenticationCredential(viewController vc: UIViewController? = nil) -> Observable<FacebookModel> {
        self.loginManager = LoginManager()
        return self.loginManager!.rx
            .logInAndToken(with: self.readPermissions, loginBehavior: self.loginBehavior, viewController: vc)
            .flatMap { [weak self] token -> Observable<FacebookModel> in
                guard let weakSelf = self else {
                    return Observable.empty()
                }
                return GraphRequest(graphPath: weakSelf.graphPath,
                    parameters: weakSelf.parameters)
                    .start(token: token)
        }
    }
}
