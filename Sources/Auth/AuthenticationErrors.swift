//
//  AuthenticationErrors.swift
//
//  Created by Jorge Villalobos on 29/05/17.
//  Copyright © 2017 Jorge Villalobos. All rights reserved.
//

import CommonsAssets
import FirebaseAuth
import Foundation

public enum AuthenticationErrors: IError {
    case validatePassword
    case undefinedUser
    case unsuccessfulUser
    case userCreationFailed
    case linkUserWithFacebook
    case accountExistsWithFacebook
    case invalidPassword(NSError)
    case userDisabled(NSError)
    case emailAlreadyInUse(NSError)
    case invalidEmail(NSError)
    case wrongPassword(NSError)
    case userNotFound(NSError)
    case accountExistsWithDifferentCredential(NSError)
    case networkError(NSError)
    case credentialAlreadyInUse(NSError)
    case unknown(NSError)

    public init(_ nsError: NSError) {
        let errorCode: Int = nsError.code
        guard let authError = AuthErrorCode(rawValue: errorCode) else {
            self = .unknown(nsError)
            return
        }
        switch authError {
        case .userDisabled:
            self = .userDisabled(nsError)
        case .emailAlreadyInUse:
            self = .emailAlreadyInUse(nsError)
        case .invalidEmail:
            self = .invalidEmail(nsError)
        case .wrongPassword:
            self = .wrongPassword(nsError)
        case .userNotFound:
            self = .userNotFound(nsError)
        case .accountExistsWithDifferentCredential:
            self = .accountExistsWithDifferentCredential(nsError)
        case .networkError:
            self = .networkError(nsError)
        case .credentialAlreadyInUse:
            self = .credentialAlreadyInUse(nsError)
        case .weakPassword:
            self = .invalidPassword(nsError)
        default:
            self = .unknown(nsError)
            break
        }
    }

    public var bundleId: String {
        return DatabaseProxyConstants.error.description
    }

    public var code: Int {
        switch self {
        case .validatePassword:
            return 100
        case .undefinedUser:
            return 200
        case .unsuccessfulUser:
            return 300
        case .userCreationFailed:
            return 400
        case .linkUserWithFacebook:
            return 500
        case .accountExistsWithFacebook:
            return 600
        case let .invalidPassword(error):
            return error.code
        case let .userDisabled(error):
            return error.code
        case let .emailAlreadyInUse(error):
            return error.code
        case let .invalidEmail(error):
            return error.code
        case let .wrongPassword(error):
            return error.code
        case let .userNotFound(error):
            return error.code
        case let .accountExistsWithDifferentCredential(error):
            return error.code
        case let .networkError(error):
            return error.code
        case let .credentialAlreadyInUse(error):
            return error.code
        case let .unknown(error):
            return error.code
        }
    }

    public var domain: String {
        return DatabaseProxyConstants.DomainErrors.auth.description
    }

    public func messageError(_ args: [CVarArg]) -> String {
        switch self {
        case let .unknown(error):
            return error.localizedDescription
        default:
            let message: String! = BundleFacade.shared.displayMessage(bundleId: bundleId, key: key, args: args)
            return message
        }
    }
}
