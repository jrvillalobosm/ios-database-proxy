//
//  User+Extensions.swift
//
//  Created by Jorge Villalobos on 18/10/18.
//  Copyright © 2018 Jorge Villalobos. All rights reserved.
//

import FirebaseAuth
import Foundation
import RxSwift

public typealias AuthenticationCredential = AuthCredential

public extension User {
    public var currentEmail: String {
        guard let username = self.email else {
            return "Undefined"
        }
        return username
    }
}

extension Reactive where Base: User {
    public func getIDToken() -> Observable<String> {
        return Observable.create { observer in
            self.base.getIDToken { token, error in
                guard let error = error else {
                    if let token = token {
                        observer.onNext(token)
                    }
                    observer.onCompleted()
                    return
                }
                observer.onError(AuthenticationErrors(error as NSError))
            }
            return Disposables.create()
        }
    }

    public func updatePassword(to password: String) -> Observable<Void> {
        return Observable.create { observer in
            self.base.updatePassword(to: password) { error in
                guard let error = error else {
                    observer.onNext(())
                    observer.onCompleted()
                    return
                }
                observer.onError(AuthenticationErrors(error as NSError))
            }
            return Disposables.create()
        }
    }
    
    public func reauthenticateAndRetrieveData(with credential: AuthenticationCredential) -> Observable<AuthDataResult> {
        return Observable.create { observer in
            self.base.reauthenticateAndRetrieveData(with: credential) { result, error in
                guard let error = error else {
                    if let result = result {
                        observer.onNext(result)
                    }
                    observer.onCompleted()
                    return
                }
                observer.onError(AuthenticationErrors(error as NSError))
            }
            return Disposables.create()
        }
    }

    public func linkAndRetrieveData(with credential: AuthenticationCredential) -> Observable<AuthDataResult> {
        return Observable.create { observer in
            self.base.linkAndRetrieveData(with: credential) { result, error in
                guard error != nil else {
                    if let result = result {
                        observer.onNext(result)
                    }
                    observer.onCompleted()
                    return
                }
                observer.onError(AuthenticationErrors.linkUserWithFacebook)
            }
            return Disposables.create()
        }
    }

    public func unlink(fromProvider provider: String) -> Observable<User> {
        return Observable.create { observer in
            self.base.unlink(fromProvider: provider) { user, error in
                guard let error = error else {
                    if let user = user {
                        observer.onNext(user)
                    }
                    observer.onCompleted()
                    return
                }
                observer.onError(AuthenticationErrors(error as NSError))
            }
            return Disposables.create()
        }
    }

    public func sendEmailVerification(with settings: ActionCodeSettings? = nil) -> Observable<Void> {
        return Observable.create { observer in
            let completion: SendEmailVerificationCallback = { error in
                guard let error = error else {
                    observer.onNext(())
                    observer.onCompleted()
                    return
                }
                observer.onError(AuthenticationErrors(error as NSError))
            }
            if let settings = settings {
                self.base.sendEmailVerification(with: settings, completion: completion)
            } else {
                self.base.sendEmailVerification(completion: completion)
            }
            return Disposables.create()
        }
    }

    public func delete() -> Observable<Void> {
        return Observable.create { observer in
            self.base.delete { error in
                guard let error = error else {
                    observer.onNext(())
                    observer.onCompleted()
                    return
                }
                observer.onError(AuthenticationErrors(error as NSError))
            }
            return Disposables.create()
        }
    }
}
