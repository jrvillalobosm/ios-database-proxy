//
//  Authlayer.swift
//
//  Created by Jorge Villalobos on 18/10/18.
//  Copyright © 2018 Jorge Villalobos. All rights reserved.
//

import CommonsAssets
import FirebaseAuth
import Foundation
import Logger
import RxSwift

public typealias AuthUser = User

public extension Notification.Name {
    static let authenticatedUserLoggedIn = Notification.Name("authenticatedUserLoggedIn")
    static let authenticatedUserAnonymous = Notification.Name("authenticatedUserAnonymous")
    static let authenticatedUserUnlogged = Notification.Name("authenticatedUserUnlogged")
}

public class Authlayer {
    private let log = Logger.getLogger("Authlayer")

    public init() { }

    private var auth: Auth {
        return Auth.auth()
    }
    
    public enum NotificationManager {
        case anonymous
        case logged
        case unLogged
        
        public func send(userInfo: [AnyHashable : Any] = [:]) {
            switch self {
            case .anonymous:
                let userInfo = userInfo + ["userId": Authlayer.user.uid]
                NotificationCenter.default
                    .post(name: .authenticatedUserAnonymous, object: self, userInfo: userInfo)
            case .logged:
                let userInfo = userInfo + ["userId": Authlayer.user.uid]
                NotificationCenter.default
                    .post(name: .authenticatedUserLoggedIn, object: self, userInfo: userInfo)
            case .unLogged:
                NotificationCenter.default.post(name: .authenticatedUserUnlogged, object: self, userInfo: userInfo)
            }
        }
    }
    
    public func sendNotification(userInfo: [AnyHashable : Any] = [:]) {
        switch Authlayer.user.statusAuth {
        case .anonymous:
            self.log.debug("Usuario Autenticado como Anónimo ==> \(Authlayer.user.uid)")
            NotificationManager.anonymous.send(userInfo: userInfo)
        case .logged:
            self.log.debug("Usuario Autenticado ==> \(Authlayer.user.uid)")
            NotificationManager.logged.send(userInfo: userInfo)
        case .notLogged:
            self.log.debug("Usuario NO Autenticado")
            NotificationManager.unLogged.send(userInfo: userInfo)
        }
    }

    public var stateDidChange: Observable<AuthUser?> {
        return self.auth.rx.addStateDidChangeListener()
    }

    public func sendPasswordReset(withEmail email: String,
        actionCodeSettings: ActionCodeSettings? = nil
    ) -> Observable<Void> {
        return self.auth.rx.sendPasswordReset(withEmail: email, actionCodeSettings: actionCodeSettings)
    }

    public func fetchProviders(forEmail email: String) -> Observable<[String]> {
        return self.auth.rx.fetchProviders(forEmail: email)
    }

    public func createUser(email: String, password: String) -> Observable<AuthUser> {
        return self.auth.rx.createUser(withEmail: email, password: password)
            .map { result -> AuthUser in
                result.user
        }
    }

    public func signOut() -> Observable<Bool> {
        switch Authlayer.user.statusAuth {
        case .anonymous:
            return Authlayer.user.signOut()
        case .logged:
            return Observable.zip(Authlayer.user.signOut(), Authlayer.facebook.logOut().map({ _ in true }))
                .map { (signOut, logOut) -> Bool in
                    signOut && logOut
            }
        default:
            return Observable.just(false)
        }
    }

    public func signInAnonymously() -> Observable<AuthUser> {
        let signInAnonymouslyObservable: Observable<AuthUser> = self.auth.rx.signInAnonymously()
            .map { result -> AuthUser in
                return result.user
        }
        guard let currentUser = self.auth.currentUser else {
            return signInAnonymouslyObservable
        }
        return (currentUser.isAnonymous)
            ? Observable.just(currentUser)
            : self.signOut().flatMap { _ -> Observable<AuthUser> in
                signInAnonymouslyObservable
        }
    }

    public func signInAndRetrieveData(with credential: AuthenticationCredential
    ) -> Observable<AuthUser> {
        return self.auth.rx.signInAndRetrieveData(with: credential)
            .map { result -> AuthUser in
                result.user
        }
    }

    public func signIn(withEmail email: String,
        password: String
    ) -> Observable<AuthUser> {
        return self.auth.rx.signIn(withEmail: email, password: password)
            .map { result -> AuthUser in
                result.user
            }
    }

    public func deleteAnonymousUser() -> Observable<Bool> {
        if let currentUser = Authlayer.user.currentUser, currentUser.isAnonymous {
            return Authlayer.user.delete()
        } else {
            return Observable.just(false)
        }
    }
}

public extension Authlayer {
    static let shared: Authlayer = {
        Authlayer()
    }()

    static let user: AuthenticationUser = {
        AuthenticationUser()
    }()

    static let facebook: FacebookUser = {
        FacebookUser()
    }()
}
