//
//  UserProfileChangeRequest+Extensions.swift
//
//  Created by Jorge Villalobos on 11/02/19.
//  Copyright © 2019 Jorge Villalobos. All rights reserved.
//

import FirebaseAuth
import Foundation
import RxSwift

extension Reactive where Base: UserProfileChangeRequest {

    public func commitChanges() -> Observable<Void> {
        return Observable.create { observer in
            self.base.commitChanges { error in
                guard let error = error else {
                    observer.onNext(())
                    observer.onCompleted()
                    return
                }
                observer.onError(AuthenticationErrors(error as NSError))
            }
            return Disposables.create()
        }
    }
}
