//
//  DataSnapshot+Extensions.swift
//
//  Created by Jorge Villalobos on 05/06/17.
//  Copyright © 2017 Jorge Villalobos. All rights reserved.
//

import CommonsAssets
import FirebaseDatabase
import Foundation
import ObjectMapper

public extension DataSnapshot {
    public var toJson: Json {
        var obj: Json = value as! Json
        obj["uuid"] = key as AnyObject
        return obj
    }

    public func getMappableModel<T: Mappable>() -> T {
        return Mapper<T>().map(JSON: self.toJson)!
    }

    public func getModel<T: IModel>() -> T {
        let model: T = self.getMappableModel()
        return model
    }
}
