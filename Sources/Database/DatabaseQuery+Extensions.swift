//
//  DatabaseQuery+Extensions.swift
//
//  Created by Jorge Villalobos on 10/09/18.
//  Copyright © 2018 Jorge Villalobos. All rights reserved.
//

import CommonsAssets
import FirebaseDatabase
import Foundation
import RxSwift

public typealias PreviousSiblingKeyDataSnapshot = (snapshot: DatalayerSnapshot, prevKey: String?)

public enum QueryOrdered {
    case key
    case value
    case priority
    case child(String)
}

public enum QueryLimited {
    case last(UInt)
    case first(UInt)
}

public enum Query {
    case ending(atValue: Any?, childKey: String?)
    case equal(toValue: Any?, childKey: String?)
    case starting(atValue: Any?, childKey: String?)
    case limited(to: QueryLimited)
    case ordered(by: QueryOrdered)
}

extension Reactive where Base: DatabaseQuery {
    public func observeSingleEvent(_ eventType: DatalayerEventType) -> Observable<DatalayerSnapshot> {
        return Observable.create { observer in
            self.base.observeSingleEvent(of: eventType, with: { snapshot in
                observer.onNext(snapshot)
                observer.onCompleted()
            }, withCancel: { error in
                    observer.onError(DatabaseError(error as NSError))
                })
            return Disposables.create()
        }
    }

    public func observeSingleEvent<T>(_ eventType: DatalayerEventType) -> Observable<T> {
        return self.observeSingleEvent(eventType)
            .flatMap { snapshot -> Observable<T> in
                if snapshot.exists() {
                    return Observable.of(snapshot.value as! T)
                } else {
                    return Observable.error(DatabaseError.modelDoesNotExist)
                }
        }
    }

    public func observeSingleEvent<T: IModel>(_ eventType: DatalayerEventType) -> Observable<T> {
        return self.observeSingleEvent(eventType)
            .flatMap { snapshot -> Observable<T> in
                if snapshot.exists() {
                    return Observable.of(snapshot.getModel())
                } else {
                    return Observable.error(DatabaseError.modelDoesNotExist)
                }
        }
    }

    public func observeSingleEventAndPreviousSiblingKey(
        _ eventType: DatalayerEventType
    ) -> Observable<PreviousSiblingKeyDataSnapshot> {
        return Observable.create { observer in
            self.base.observeSingleEvent(of: eventType, andPreviousSiblingKeyWith: { snapshot, prevKey in
                observer.onNext(PreviousSiblingKeyDataSnapshot(snapshot, prevKey))
                observer.onCompleted()
            }, withCancel: { error in
                    observer.onError(DatabaseError(error as NSError))
                })
            return Disposables.create()
        }
    }
}

extension Reactive where Base: DatabaseQuery {
    public func observeEvent(_ eventType: DatalayerEventType) -> Observable<DatalayerSnapshot> {
        return Observable.create { observer in
            let handle = self.base.observe(eventType, with: { snapshot in
                observer.onNext(snapshot)
            }, withCancel: { error in
                    observer.onError(DatabaseError(error as NSError))
                })
            return Disposables.create {
                self.base.removeObserver(withHandle: handle)
            }
        }
    }

    public func observeEvent<T: IModel>(_ eventType: DatalayerEventType) -> Observable<T> {
        return self.observeEvent(eventType)
            .flatMap { snapshot -> Observable<T> in
                if snapshot.exists() {
                    return Observable.of(snapshot.getModel())
                } else {
                    return Observable.error(DatabaseError.modelDoesNotExist)
                }
        }
    }

    public func observeEventAndPreviousSiblingKey(
        _ eventType: DatalayerEventType
    ) -> Observable<PreviousSiblingKeyDataSnapshot> {
        return Observable.create { observer in
            let handle = self.base.observe(eventType, andPreviousSiblingKeyWith: { snapshot, prevKey in
                observer.onNext(PreviousSiblingKeyDataSnapshot(snapshot, prevKey))
            }, withCancel: { error in
                    observer.onError(DatabaseError(error as NSError))
                })
            return Disposables.create {
                self.base.removeObserver(withHandle: handle)
            }
        }
    }
}

public extension DatalayerQuery {
    public func query(_ type: Query) -> DatalayerQuery {
        switch type {
        case let .ending(value, key):
            return self.queryEnding(atValue: value, childKey: key)
        case let .equal(value, key):
            return self.queryEqual(toValue: value, childKey: key)
        case let .starting(value, key):
            return self.queryStarting(atValue: value, childKey: key)
        case let .limited(limited):
            switch limited {
            case let .last(value):
                return self.queryLimited(toLast: value)
            case let .first(value):
                return self.queryLimited(toFirst: value)
            }
        case let .ordered(ordered):
            switch ordered {
            case let .child(value):
                return self.queryOrdered(byChild: value)
            case .key:
                return self.queryOrderedByKey()
            case .value:
                return self.queryOrderedByValue()
            case .priority:
                return self.queryOrderedByPriority()
            }
        }
    }
}
