//
//  DatalayerErrors.swift
//
//  Created by Jorge Villalobos on 29/05/17.
//  Copyright © 2017 Jorge Villalobos. All rights reserved.
//

import CommonsAssets
import Foundation

public enum DatabaseError: IError {
    case modelDoesNotExist
    case unknown(NSError)

    public init(_ error: NSError) {
        let rawValue: Int = error.code
        switch rawValue {
        default: self = .unknown(error)
        }
    }

    public var bundleId: String {
        return DatabaseProxyConstants.error.description
    }

    public var code: Int {
        switch self {
        case .modelDoesNotExist:
            return 1000
        default:
            return 0
        }
    }

    public var domain: String {
        return DatabaseProxyConstants.DomainErrors.database.description
    }

    public func messageError(_ args: [CVarArg]) -> String {
        switch self {
        case let .unknown(error):
            return error.localizedDescription
        default:
            let message: String! = BundleFacade.shared.displayMessage(bundleId: bundleId, key: key, args: args)
            return message
        }
    }
}
