//
//  Datalayer.swift
//
//  Created by Jorge Villalobos on 06/09/18.
//  Copyright © 2018 Jorge Villalobos. All rights reserved.
//

import CommonsAssets
import FirebaseDatabase
import Foundation
import RxSwift

public typealias DatalayerSnapshot = DataSnapshot
public typealias DatalayerEventType = DataEventType
public typealias DatalayerReference = DatabaseReference
public typealias DatalayerQuery = DatabaseQuery

public class Datalayer {
    fileprivate let databaseReference: DatalayerReference

    public var onDisconnectRemoveValue: Observable<DatalayerReference> {
        return self.databaseReference.rx.onDisconnectRemoveValue()
    }

    public init(_ databaseRef: DatalayerReference) {
        self.databaseReference = databaseRef
    }

    public init(_ databasePath: PatternFormatter, message: Json = [:], generateAutoId: Bool) {
        let reference = Database.database().reference(withPath: try! databasePath.formatter(message: message))
        self.databaseReference = generateAutoId ? reference.childByAutoId() : reference
    }

    public func get(eventType: DatalayerEventType = .value,
        from datalayerPath: PatternFormatter? = nil,
        message: Json = [:],
        isSingleEvent: Bool = false
    ) -> Observable<DatalayerSnapshot> {
        let reference = self.getDatalayerReference(from: datalayerPath, message: message)
        var observable: Observable<DatalayerSnapshot>
        if isSingleEvent {
            observable = reference.rx.observeSingleEvent(eventType)
        } else {
            observable = reference.rx.observeEvent(eventType)
        }
        return observable
    }

    public func update(from datalayerPath: PatternFormatter? = nil,
        message: Json = [:],
        data: [AnyHashable: Any]
    ) -> Observable<DatalayerReference> {
        let reference = self.getDatalayerReference(from: datalayerPath, message: message)
        return reference.rx.updateChildValues(data)
    }

    public func set(from datalayerPath: PatternFormatter? = nil,
        message: Json = [:],
        value: Any?,
        andPriority priority: Any? = nil
    ) -> Observable<DatalayerReference> {
        let reference = self.getDatalayerReference(from: datalayerPath, message: message)
        return reference.rx.setValue(value, andPriority: priority)
    }

    public func delete(from datalayerPath: PatternFormatter? = nil,
        message: Json = [:]
    ) -> Observable<DatalayerReference> {
        let reference = self.getDatalayerReference(from: datalayerPath, message: message)
        return reference.rx.removeValue()
    }
}

public extension Datalayer {
    public func get<T: IModel>(eventType: DatalayerEventType = .value,
        from datalayerPath: PatternFormatter? = nil,
        message: Json = [:],
        isSingleEvent: Bool = false,
        withEmptyValue: Bool = true
    ) -> Observable<T> {
        let reference = self.getDatalayerReference(from: datalayerPath, message: message)
        var observable: Observable<T>
        if isSingleEvent {
            observable = reference.rx.observeSingleEvent(eventType)
        } else {
            observable = reference.rx.observeEvent(eventType)
        }
        return observable
            .catchError { error -> Observable<T> in
                if withEmptyValue, let error: DatabaseError = error as? DatabaseError, case .modelDoesNotExist = error {
                    return Observable.empty()
                }
                return Observable.error(error)
        }
    }

    public func update<T: IModel>(from datalayerPath: PatternFormatter? = nil,
        message: Json = [:],
        model: T
    ) -> Observable<DatalayerReference> {
        let reference = self.getDatalayerReference(from: datalayerPath, message: message)
        return reference.rx.updateChildValues(model)
    }

    public func set<T: IModel>(from datalayerPath: PatternFormatter? = nil,
        message: Json = [:],
        model: T,
        andPriority priority: Any? = nil
    ) -> Observable<DatalayerReference> {
        let reference = self.getDatalayerReference(from: datalayerPath, message: message)
        return reference.rx.setValue(model, andPriority: priority)
    }
}

public extension Datalayer {
    public func get<T>(eventType: DatalayerEventType = .value,
        from datalayerPath: PatternFormatter? = nil,
        message: Json = [:],
        withEmptyValue: Bool = true
    ) -> Observable<T> {
        let reference = self.getDatalayerReference(from: datalayerPath, message: message)
        var observable: Observable<T>
        observable = reference.rx.observeSingleEvent(eventType)
        return observable
            .catchError { error -> Observable<T> in
                if withEmptyValue, let error: DatabaseError = error as? DatabaseError, case .modelDoesNotExist = error {
                    return Observable.empty()
                }
                return Observable.error(error)
        }
    }
}

public extension Datalayer {
    public func query(from datalayerPath: PatternFormatter? = nil,
        message: Json = [:],
        queries: [Query] = [.ordered(by: .key)]
    ) -> DatalayerQuery {
        let reference = self.getDatalayerReference(from: datalayerPath, message: message)
        var result: DatalayerQuery?
        for query in queries {
            if let qq = result {
                result = qq.query(query)
            } else {
                result = reference.query(query)
            }
        }
        return result!
    }

    public func getDatalayerReference(from databasePath: PatternFormatter? = nil,
        message: Json = [:],
        generateAutoId: Bool = false
    ) -> DatalayerReference {
        if let databasePath = databasePath {
            return self.getDatalayerReference(from: try! databasePath.formatter(message: message),
                generateAutoId: generateAutoId)
        } else {
            return (generateAutoId) ? self.databaseReference.childByAutoId() : self.databaseReference
        }
    }

    public func getDatalayerReference(from databasePath: String? = nil,
        generateAutoId: Bool = false
    ) -> DatalayerReference {
        let reference: DatalayerReference
        if let databasePath = databasePath {
            reference = self.databaseReference.child(databasePath)
        } else {
            reference = self.databaseReference
        }
        return (generateAutoId) ? reference.childByAutoId() : reference
    }
}

public extension Datalayer {
    static let main: Datalayer = {
        Datalayer(Database.database().reference())
    }()

    public static func create(with path: String) -> Datalayer {
        return Datalayer(Database.database().reference(withPath: path))
    }

    public static func create(databasePath: PatternFormatter,
        message: Json = [:],
        generateAutoId: Bool = false
    ) -> Datalayer {
        return Datalayer(databasePath, message: message, generateAutoId: generateAutoId)
    }

    public func child(from databasePath: PatternFormatter? = nil,
        message: Json = [:],
        generateAutoId: Bool = false
    ) -> Datalayer {
        let reference = self.getDatalayerReference(from: databasePath, message: message, generateAutoId: generateAutoId)
        return Datalayer(reference)
    }

    public func child(from pathString: String? = nil, generateAutoId: Bool = false) -> Datalayer {
        let reference = self.getDatalayerReference(from: pathString, generateAutoId: generateAutoId)
        return Datalayer(reference)
    }
}
