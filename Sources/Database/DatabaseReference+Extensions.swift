//
//  DatabaseReference+Extensions.swift
//
//  Created by Jorge Villalobos on 10/09/18.
//  Copyright © 2018 Jorge Villalobos. All rights reserved.
//

import CommonsAssets
import FirebaseDatabase
import Foundation
import RxSwift

public typealias DatabaseReferenceTransactionResult = (committed: Bool, snapshot: DatalayerSnapshot?)

extension Reactive where Base: DatabaseReference {
    public func setValue(_ value: Any?, andPriority priority: Any? = nil) -> Observable<DatabaseReference> {
        return Observable.create { observer in
            self.base.setValue(value, andPriority: priority) { error, firDatabaseReference in
                guard let error = error else {
                    observer.onNext(firDatabaseReference)
                    observer.onCompleted()
                    return
                }
                observer.onError(DatabaseError(error as NSError))
            }
            return Disposables.create()
        }
    }

    public func setValue<T: IModel>(_ model: T, andPriority priority: Any? = nil) -> Observable<DatabaseReference> {
        return self.setValue(model.toModel(), andPriority: priority)
    }

    public func removeValue() -> Observable<DatabaseReference> {
        return Observable.create { observer in
            self.base.removeValue() { error, firDatabaseReference in
                guard let error = error else {
                    observer.onNext(firDatabaseReference)
                    observer.onCompleted()
                    return
                }
                observer.onError(DatabaseError(error as NSError))
            }
            return Disposables.create()
        }
    }

    public func setPriority(_ priority: Any?) -> Observable<DatabaseReference> {
        return Observable.create { observer in
            self.base.setPriority(priority) { error, firDatabaseReference in
                guard let error = error else {
                    observer.onNext(firDatabaseReference)
                    observer.onCompleted()
                    return
                }
                observer.onError(DatabaseError(error as NSError))
            }
            return Disposables.create()
        }
    }

    public func updateChildValues(_ values: [AnyHashable: Any]) -> Observable<DatabaseReference> {
        return Observable.create { observer in
            self.base.updateChildValues(values) { error, firDatabaseReference in
                guard let error = error else {
                    observer.onNext(firDatabaseReference)
                    observer.onCompleted()
                    return
                }
                observer.onError(DatabaseError(error as NSError))
            }
            return Disposables.create()
        }
    }

    public func updateChildValues<T: IModel>(_ model: T) -> Observable<DatabaseReference> {
        return self.updateChildValues(model.toModel())
    }

    public func onDisconnectSetValue(_ value: Any?, andPriority priority: Any? = nil) -> Observable<DatabaseReference> {
        return Observable.create { observer in
            self.base.onDisconnectSetValue(value, andPriority: priority) { error, firDatabaseReference in
                guard let error = error else {
                    observer.onNext(firDatabaseReference)
                    observer.onCompleted()
                    return
                }
                observer.onError(DatabaseError(error as NSError))
            }
            return Disposables.create()
        }
    }

    public func onDisconnectRemoveValue() -> Observable<DatabaseReference> {
        return Observable.create { observer in
            self.base.onDisconnectRemoveValue() { error, firDatabaseReference in
                guard let error = error else {
                    observer.onNext(firDatabaseReference)
                    observer.onCompleted()
                    return
                }
                observer.onError(DatabaseError(error as NSError))
            }
            return Disposables.create()
        }
    }

    public func onDisconnectUpdateChildValues(_ values: [AnyHashable: Any]) -> Observable<DatabaseReference> {
        return Observable.create { observer in
            self.base.onDisconnectUpdateChildValues(values) { error, firDatabaseReference in
                guard let error = error else {
                    observer.onNext(firDatabaseReference)
                    observer.onCompleted()
                    return
                }
                observer.onError(DatabaseError(error as NSError))
            }
            return Disposables.create()
        }
    }

    public func cancelDisconnectOperations() -> Observable<DatabaseReference> {
        return Observable.create { observer in
            self.base.cancelDisconnectOperations() { error, firDatabaseReference in
                guard let error = error else {
                    observer.onNext(firDatabaseReference)
                    observer.onCompleted()
                    return
                }
                observer.onError(DatabaseError(error as NSError))
            }
            return Disposables.create()
        }
    }

    public func runTransactionBlock(_ block: @escaping (MutableData) -> TransactionResult,
        withLocalEvents: Bool
    ) -> Observable<DatabaseReferenceTransactionResult> {
        return Observable.create { observer in
            self.base.runTransactionBlock(block, andCompletionBlock: { error, committed, snapshot in
                guard let error = error else {
                    observer.onNext(DatabaseReferenceTransactionResult(committed, snapshot))
                    observer.onCompleted()
                    return
                }
                observer.onError(DatabaseError(error as NSError))
            }, withLocalEvents: withLocalEvents)
            return Disposables.create()
        }
    }

    public func runTransactionBlock(
        _ block: @escaping (MutableData) -> TransactionResult
    ) -> Observable<DatabaseReferenceTransactionResult> {
        return self.runTransactionBlock(block, withLocalEvents: true)
    }
}
