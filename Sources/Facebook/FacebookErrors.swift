//
//  FacebookErrors.swift
//
//  Created by Jorge Villalobos on 13/11/18.
//  Copyright © 2018 Jorge Villalobos. All rights reserved.
//

import CommonsAssets
import Foundation

public enum FacebookErrors: IError {
    case unavailableService
    case requiredEmailPermission
    case unknown(NSError)

    public init(_ error: NSError) {
        let rawValue: Int = error.code
        switch rawValue {
        default: self = .unavailableService
        }
    }

    public var bundleId: String {
        return DatabaseProxyConstants.error.description
    }

    public var code: Int {
        switch self {
        case .unavailableService:
            return 1001
        case .requiredEmailPermission:
            return 9999
        default:
            return 0
        }
    }

    public var domain: String {
        return DatabaseProxyConstants.DomainErrors.facebook.description
    }

    public func messageError(_ args: [CVarArg]) -> String {
        switch self {
        case let .unknown(error):
            return error.localizedDescription
        default:
            let message: String! = BundleFacade.shared.displayMessage(bundleId: bundleId, key: key, args: args)
            return message
        }
    }
}
