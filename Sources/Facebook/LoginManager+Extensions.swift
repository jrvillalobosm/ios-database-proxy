//
//  LoginManager+Extensions.swift
//
//  Created by Jorge Villalobos on 12/02/18.
//  Copyright © 2018 Jorge Villalobos. All rights reserved.
//

import FacebookCore
import FacebookLogin
import Foundation
import RxSwift

extension Reactive where Base: LoginManager {
    public func logInAndToken(with readPermissions: [ReadPermission] = [.publicProfile],
        loginBehavior: LoginBehavior = .native,
        viewController: UIViewController? = nil
    ) -> Observable<String> {
        self.base.loginBehavior = loginBehavior
        return self.logIn(with: readPermissions, viewController: viewController)
            .flatMap { loginResult -> Observable<String> in
                switch loginResult {
                case .cancelled, .failed:
                    return Observable.empty()
                case let .success(_, declined, accessToken):
                    let token = accessToken.authenticationToken
                    if declined.contains(Permission(name: "email")) {
                        return Observable.error(FacebookErrors.requiredEmailPermission)
                    } else {
                        return Observable.just(token)
                    }
                }
            }.filter({ !$0.isEmpty })
    }

    public func logIn(with readPermissions: [ReadPermission],
        viewController: UIViewController? = nil
    ) -> Observable<LoginResult> {
        return Observable.create { observer in
            self.base.logIn(readPermissions: readPermissions, viewController: viewController) { loginResult in
                if case let .failed(error) = loginResult {
                    observer.onError(error)
                } else {
                    observer.onNext(loginResult)
                    observer.onCompleted()
                }
            }
            return Disposables.create()
        }
    }

    public func logIn(with publishPermissions: [PublishPermission],
        viewController: UIViewController? = nil
    ) -> Observable<LoginResult> {
        return Observable.create { observer in
            self.base.logIn(publishPermissions: publishPermissions, viewController: viewController) { loginResult in
                if case let .failed(error) = loginResult {
                    observer.onError(error)
                } else {
                    observer.onNext(loginResult)
                    observer.onCompleted()
                }
            }
            return Disposables.create()
        }
    }

    public func logOut() -> Observable<Void> {
        return Observable.create { observer in
            self.base.logOut()
            observer.onNext(())
            observer.onCompleted()
            return Disposables.create()
        }
    }
}

extension LoginManager: ReactiveCompatible { }
