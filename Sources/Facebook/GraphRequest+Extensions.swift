//
//  GraphRequest+Extensions.swift
//
//  Created by Jorge Villalobos on 12/02/18.
//  Copyright © 2018 Jorge Villalobos. All rights reserved.
//

import HttpNetworkLayer
import CommonsAssets
import FacebookCore
import Foundation
import RxSwift

public extension Reactive where Base: GraphRequestProtocol {
    public func start() -> Observable<GraphRequestResult<GraphRequest>> {
        return Observable.create { observer in
            self.base.start { httpResponse, result in
                guard httpResponse!.statusCode == 200 else {
                    observer.onError(HttpNetworkingError.requestDontFulfilled(httpResponse!.statusCode))
                    return
                }
                if case let GraphRequestResult.failed(error) = result {
                    observer.onError(FacebookErrors(error as NSError))
                    return
                } else if let result = result as? GraphRequestResult<GraphRequest> {
                    observer.onNext(result)
                    observer.onCompleted()
                }
                observer.onCompleted()
            }
            return Disposables.create()
        }
    }
}

extension GraphRequest: ReactiveCompatible { }

public extension GraphRequest {
    public func start(token: String) -> Observable<FacebookModel> {
        return self.rx.start().flatMap { result -> Observable<Json> in
            switch result {
            case let .failed(error):
                return Observable.error(FacebookErrors(error as NSError))
            case let .success(response):
                guard let objectMap = response.dictionaryValue else {
                    return Observable.error(FacebookErrors.unavailableService)
                }
                return Observable.just(objectMap)
            }
        }.map({ FacebookModel(token: token, data: $0) })
    }
}
