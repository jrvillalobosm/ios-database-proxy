//
//  FacebookModel.swift
//
//  Created by Jorge Villalobos on 12/02/18.
//  Copyright © 2018 Jorge Villalobos. All rights reserved.
//

import CommonsAssets
import Foundation

public class FacebookModel {
    public var token: String
    public var userName: String
    public var email: String
    public var urlPicture: String
    public var credential: AuthenticationCredential?

    public init(token: String, data: Json) {
        self.token = token
        self.userName = data["name"] as! String
        self.email = data["email"] as! String
        let picture = data["picture"] as! Json
        let dataPicture = picture["data"] as! Json
        self.urlPicture = dataPicture["url"] as! String
    }
}
