//
//  StorageErrors.swift
//
//  Created by Jorge Villalobos on 05/06/17.
//  Copyright © 2017 Jorge Villalobos. All rights reserved.
//

import CommonsAssets
import FirebaseStorage
import Foundation

public enum StorageError: IError {
    case objectNotFound(NSError)
    case bucketNotFound(NSError)
    case projectNotFound(NSError)
    case quotaExceeded(NSError)
    case unauthenticated(NSError)
    case unauthorized(NSError)
    case retryLimitExceeded(NSError)
    case nonMatchingChecksum(NSError)
    case downloadSizeExceeded(NSError)
    case cancelled(NSError)
    case unknown(NSError)

    public init(_ nsError: NSError) {
        let errorCode: Int = nsError.code
        guard let storageError = StorageErrorCode(rawValue: errorCode) else {
            self = .unknown(nsError)
            return
        }
        switch storageError {
        case .objectNotFound:
            self = .objectNotFound(nsError)
        case .bucketNotFound:
            self = .bucketNotFound(nsError)
        case .projectNotFound:
            self = .projectNotFound(nsError)
        case .quotaExceeded:
            self = .quotaExceeded(nsError)
        case .unauthenticated:
            self = .unauthenticated(nsError)
        case .unauthorized:
            self = .unauthorized(nsError)
        case .retryLimitExceeded:
            self = .retryLimitExceeded(nsError)
        case .nonMatchingChecksum:
            self = .nonMatchingChecksum(nsError)
        case .downloadSizeExceeded:
            self = .downloadSizeExceeded(nsError)
        case .cancelled:
            self = .cancelled(nsError)
        default:
            self = .unknown(nsError)
            break
        }
    }

    public var bundleId: String {
        return DatabaseProxyConstants.error.description
    }

    public var code: Int {
        switch self {
        case .objectNotFound:
            return 10001
        case .bucketNotFound:
            return 10002
        case .projectNotFound:
            return 10003
        case .quotaExceeded:
            return 10004
        case .unauthenticated:
            return 10005
        case .unauthorized:
            return 10006
        case .retryLimitExceeded:
            return 10007
        case .nonMatchingChecksum:
            return 10008
        case .downloadSizeExceeded:
            return 10009
        case .cancelled:
            return 10010
        default:
            return 10000
        }
    }

    public var domain: String {
        return DatabaseProxyConstants.DomainErrors.storage.description
    }

    public func messageError(_ args: [CVarArg]) -> String {
        switch self {
        case let .unknown(error):
            return error.localizedDescription
        default:
            let message: String! = BundleFacade.shared.displayMessage(bundleId: bundleId, key: key, args: args)
            return message
        }
    }
}
