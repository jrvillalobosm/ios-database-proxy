//
//  StorageObservableTask+Extensions.swift
//
//  Created by Jorge Villalobos on 11/11/18.
//  Copyright © 2018 Jorge Villalobos. All rights reserved.
//

import FirebaseStorage
import Foundation
import RxSwift

extension Reactive where Base: StorageObservableTask {
    public func observe(_ status: StorageTaskStatus) -> Observable<StorageTaskSnapshot> {
        return Observable.create { observer in
            let handle = self.base.observe(status) { snapshot in
                observer.onNext(snapshot)
            }
            return Disposables.create {
                self.base.removeObserver(withHandle: handle)
            }
        }
    }

    public func observeSuccess() -> Observable<Void> {
        let successObservable: Observable<StorageTaskSnapshot> = self.observe(.success).take(1)
        let failureObservable: Observable<StorageTaskSnapshot> = self.observe(.failure)
            .flatMap({ Observable.error(StorageError($0.error! as NSError)) })
        return Observable.of(successObservable, failureObservable).merge().map({ _ in () })
    }
}
