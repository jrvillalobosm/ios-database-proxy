//
//  StorageReference+Extensions.swift
//
//  Created by Jorge Villalobos on 08/11/18.
//  Copyright © 2018 Jorge Villalobos. All rights reserved.
//

import FirebaseStorage
import Foundation
import RxSwift

extension Reactive where Base: StorageReference {
    public func putData(_ uploadData: Data, metadata: StorageMetadata? = nil) -> Observable<StorageMetadata> {
        return Observable.create { observer in
            let task = self.base.putData(uploadData, metadata: metadata) { metadata, error in
                guard let error = error else {
                    if let metadata = metadata {
                        observer.onNext(metadata)
                    }
                    observer.onCompleted()
                    return
                }
                observer.onError(StorageError(error as NSError))
            }
            return Disposables.create {
                task.cancel()
            }
        }
    }

    public func putData(to uploadData: Data, contentType: String) -> Observable<StorageMetadata> {
        let storageMetadata = StorageMetadata()
        storageMetadata.contentType = contentType
        return self.putData(uploadData, metadata: storageMetadata)
    }

    public func putFile(from url: URL, metadata: StorageMetadata? = nil) -> Observable<StorageMetadata> {
        return Observable.create { observer in
            let task = self.base.putFile(from: url, metadata: metadata) { metadata, error in
                guard let error = error else {
                    if let metadata = metadata {
                        observer.onNext(metadata)
                    }
                    observer.onCompleted()
                    return
                }
                observer.onError(StorageError(error as NSError))
            }
            return Disposables.create {
                task.cancel()
            }
        }
    }

    public func getData(maxSize: Int64) -> Observable<Data> {
        return Observable.create { observer in
            var cancelTask: Bool = true
            let task = self.base.getData(maxSize: maxSize) { data, error in
                guard let error = error else {
                    if let data = data {
                        observer.onNext(data)
                    }
                    observer.onCompleted()
                    cancelTask = false
                    return
                }
                observer.onError(StorageError(error as NSError))
            }
            return Disposables.create {
                if cancelTask {
                    task.cancel()
                }
            }
        }
    }

    public func downloadURL() -> Observable<URL> {
        return Observable.create { observer in
            self.base.downloadURL { url, error in
                guard let error = error else {
                    if let url = url {
                        observer.onNext(url)
                    }
                    observer.onCompleted()
                    return
                }
                observer.onError(StorageError(error as NSError))
            }
            return Disposables.create()
        }
    }

    public func write(toFile url: URL) -> Observable<URL> {
        return Observable.create { observer in
            let task = self.base.write(toFile: url) { url, error in
                guard let error = error else {
                    if let url = url {
                        observer.onNext(url)
                    }
                    observer.onCompleted()
                    return
                }
                observer.onError(StorageError(error as NSError))
            }
            return Disposables.create {
                task.cancel()
            }
        }
    }

    public func getMetadata() -> Observable<StorageMetadata> {
        return Observable.create { observer in
            self.base.getMetadata { metadata, error in
                guard let error = error else {
                    if let metadata = metadata {
                        observer.onNext(metadata)
                    }
                    observer.onCompleted()
                    return
                }
                observer.onError(StorageError(error as NSError))
            }
            return Disposables.create()
        }
    }

    public func updateMetadata(_ metadata: StorageMetadata) -> Observable<StorageMetadata> {
        return Observable.create { observer in
            self.base.updateMetadata(metadata) { metadata, error in
                guard let error = error else {
                    if let metadata = metadata {
                        observer.onNext(metadata)
                    }
                    observer.onCompleted()
                    return
                }
                observer.onError(StorageError(error as NSError))
            }
            return Disposables.create()
        }
    }

    public func delete() -> Observable<Void> {
        return Observable.create { observer in
            self.base.delete { error in
                guard let error = error else {
                    observer.onNext(())
                    observer.onCompleted()
                    return
                }
                observer.onError(StorageError(error as NSError))
            }
            return Disposables.create()
        }
    }
}
