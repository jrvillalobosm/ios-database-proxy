//
//  Storagelayer.swift
//
//  Created by Jorge Villalobos on 06/09/18.
//  Copyright © 2018 Jorge Villalobos. All rights reserved.
//

import CommonsAssets
import FirebaseStorage
import Foundation
import RxSwift

public typealias StoragelayerReference = StorageReference
public typealias StoragelayerUploadTask = StorageUploadTask
public typealias StoragelayerMetadata = StorageMetadata

public protocol IDataStorage {
    var path: PatternFormatter? { get }

    var message: Json { get }

    func getData() -> Observable<Data>
}

public class Storagelayer {
    private var storageReference: StoragelayerReference

    public var maxSize: Int64 = 1024 * 1024 * 1024

    public init(_ storageRef: StoragelayerReference? = nil) {
        if let storageRef = storageRef {
            self.storageReference = storageRef
        } else {
            self.storageReference = Storage.storage().reference()
        }
    }

    public init(_ storagePath: String, withPath isPath: Bool) {
        if isPath {
            self.storageReference = Storage.storage().reference(withPath: storagePath)
        } else {
            self.storageReference = Storage.storage().reference(forURL: storagePath)
        }
    }

    public init(_ storagePath: PatternFormatter, message: Json = [:]) {
        self.storageReference = Storage.storage().reference(withPath: try! storagePath.formatter(message: message))
    }

    public func putData(from storagePath: PatternFormatter? = nil,
        message: Json = [:],
        uploadData: Data,
        metadata: StoragelayerMetadata? = nil
    ) -> Observable<Void> {
        let reference = getStoragelayerReference(from: storagePath, message: message)
        return reference.rx.putData(uploadData, metadata: metadata)
            .map({ _ in () })
    }

    public func putData(from storagePath: PatternFormatter? = nil,
        message: Json = [:],
        to uploadData: Data,
        contentType: String = "image/jpeg"
    ) -> Observable<Void> {
        let reference = getStoragelayerReference(from: storagePath, message: message)
        return reference.rx.putData(to: uploadData, contentType: contentType)
            .map({ _ in () })
    }

    public func putFile(from storagePath: PatternFormatter? = nil,
        message: Json = [:],
        url: URL,
        metadata: StoragelayerMetadata? = nil
    ) -> Observable<Void> {
        let reference = getStoragelayerReference(from: storagePath, message: message)
        return reference.rx.putFile(from: url, metadata: metadata)
            .map({ _ in () })
    }

    public func putFile(from storagePath: String? = nil,
        url: URL,
        metadata: StoragelayerMetadata? = nil
    ) -> Observable<Void> {
        let reference = getStoragelayerReference(from: storagePath)
        return reference.rx.putFile(from: url, metadata: metadata)
            .map({ _ in () })
    }

    public func putFileTask(from storagePath: PatternFormatter? = nil,
        message: Json = [:],
        url: URL,
        metadata: StoragelayerMetadata? = nil
    ) -> StoragelayerUploadTask {
        let reference = getStoragelayerReference(from: storagePath, message: message)
        return reference.putFile(from: url, metadata: metadata)
    }

    public func putFileTask(from storagePath: String? = nil,
        url: URL,
        metadata: StoragelayerMetadata? = nil
    ) -> StoragelayerUploadTask {
        let reference = getStoragelayerReference(from: storagePath)
        return reference.putFile(from: url, metadata: metadata)
    }

    public func getData(from storagePath: PatternFormatter? = nil, message: Json = [:]) -> Observable<Data> {
        let reference = getStoragelayerReference(from: storagePath, message: message)
        return reference.rx.getData(maxSize: self.maxSize)
    }

    public func getData(from storagePath: String? = nil) -> Observable<Data> {
        let reference = getStoragelayerReference(from: storagePath)
        return reference.rx.getData(maxSize: self.maxSize)
    }

    public func write(toFile localUrl: URL) -> StorageDownloadTask {
        let reference = getStoragelayerReference()
        return reference.write(toFile: localUrl)
    }

    public func getMetadata() -> Observable<StoragelayerMetadata> {
        let reference = getStoragelayerReference()
        return reference.rx.getMetadata()
    }

    public func getStoragelayerReference(from storagePath: String? = nil) -> StoragelayerReference {
        if let storagePath = storagePath {
            return self.storageReference.child(storagePath)
        } else {
            return self.storageReference
        }
    }

    public func getStoragelayerReference(from storagePath: PatternFormatter? = nil,
        message: Json = [:]
    ) -> StoragelayerReference {
        if let storagePath = storagePath {
            return self.getStoragelayerReference(from: try! storagePath.formatter(message: message))
        } else {
            return self.storageReference
        }
    }
}

public extension Storagelayer {
    static let main: Storagelayer = {
        Storagelayer()
    }()

    public static func create(with storagePath: String, withPath isPath: Bool = true) -> Storagelayer {
        return Storagelayer(storagePath, withPath: isPath)
    }

    public static func create(from storagePath: PatternFormatter? = nil, message: Json = [:]) -> Storagelayer {
        if let storagePath = storagePath {
            return Storagelayer(storagePath, message: message)
        } else {
            return Storagelayer()
        }
    }
}
