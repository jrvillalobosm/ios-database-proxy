//
//  RemoteConfigParams.swift
//
//  Created by Jorge Villalobos on 17/08/18.
//  Copyright © 2018 Jorge Villalobos. All rights reserved.
//

import Foundation

public class RemoteConfigParams {
    init() { }
}

public protocol RemoteConfigParamType {
    var _key: String { get }
}

public class RemoteConfigParam<T>: RemoteConfigParams, RemoteConfigParamType {
    public let _key: String

    public init(_ key: String) {
        self._key = key
        super.init()
    }
}

extension RemoteConfigParam: Hashable {
    public static func == (lhs: RemoteConfigParam, rhs: RemoteConfigParam) -> Bool {
        return lhs._key == rhs._key
    }

    public var hashValue: Int {
        return self._key.hashValue
    }
}

public class DecodableRemoteConfigParam<T: Decodable>: RemoteConfigParam<T> {
    public enum DataType {
        case rawData
        case json(String.Encoding)
    }

    public let dataType: DataType
    public let decoder: JSONDecoder

    public init(_ key: String, dataType: DataType = .json(.utf8), decoder: JSONDecoder = .init()) {
        self.dataType = dataType
        self.decoder = decoder
        super.init(key)
    }
}

public class CodableRemoteConfigParam<T: Codable>: DecodableRemoteConfigParam<T> {
    public let encoder: JSONEncoder

    public init(_ key: String,
        dataType: DataType = .json(.utf8),
        decoder: JSONDecoder = .init(),
        encoder: JSONEncoder = .init()
    ) {
        self.encoder = encoder
        super.init(key, dataType: dataType, decoder: decoder)
    }
}

public class AnyRemoteConfigParam: RemoteConfigParams, RemoteConfigParamType {
    public let base: RemoteConfigParamType

    public init(_ base: RemoteConfigParamType) {
        self.base = base
    }

    public var _key: String {
        return self.base._key
    }
}
