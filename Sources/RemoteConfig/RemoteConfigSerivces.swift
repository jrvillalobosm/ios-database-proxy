//
//  RemoteConfigSerivces.swift
//
//  Created by Jorge Villalobos on 15/08/18.
//  Copyright © 2018 Jorge Villalobos. All rights reserved.
//

import CommonsAssets
import FirebaseRemoteConfig
import Foundation

extension Notification.Name {
    public static let remoteConfigDidFetchConfig = Notification.Name("remoteConfigDidFetchConfig")
}

public class RemoteConfigServices {
    public static let shared = RemoteConfigServices()

    public var fetchExpirationDuration: TimeInterval = 43200.0

    public var debugMode: Bool = false {
        didSet {
            let settings = RemoteConfigSettings(developerModeEnabled: debugMode)
            RemoteConfig.remoteConfig().configSettings = settings
        }
    }

    public private(set) var fetchStatus: RemoteConfigFetchStatus = .noFetchYet

    var defaults = [String: NSObject]()

    private init() { }

    public func fetch(completion: @escaping (Error?) -> Void = { _ in }) {
        RemoteConfig.remoteConfig()
            .fetch(withExpirationDuration: self.fetchExpirationDuration) { [unowned self] status, error in
                if error == nil {
                    RemoteConfig.remoteConfig().activateFetched()
                }
                self.fetchStatus = status
                completion(error)
                NotificationCenter.default.post(name: .remoteConfigDidFetchConfig, object: error)
            }
    }

    public func setDefaults(_ defaults: [String: Any]) {
        self._setDefaults(defaults.reduce(into: [:]) {
                if let value = $1.value as? NSObject { $0[$1.key] = value }
            })
    }

    public func setDefaults(from plistFileName: String, bundle: Bundle = .main) {
        try! BundleFacade.shared.addResourceBundle(bundleId: plistFileName, bundle: bundle)
        let bundleResource = try! BundleFacade.shared.getResourceBundle(bundleId: plistFileName)
        _setDefaults(bundleResource.dict as! [String: NSObject])
    }

    func _setDefaults(_ defaults: [String: NSObject]) {
        self.defaults = defaults.reduce(into: self.defaults) { $0[$1.key] = $1.value }
        self.updateDefaults()
    }

    public func removeDefaultValue<T>(forKey key: RemoteConfigParam<T>) {
        self.defaults[key._key] = nil
        self.updateDefaults()
    }

    public func removeDefaults() {
        self.defaults = [:]
        self.updateDefaults()
    }

    func updateDefaults() {
        RemoteConfig.remoteConfig().setDefaults(self.defaults)
    }
}

public extension RemoteConfigServices {
    public subscript(_ key: RemoteConfigParam<String>) -> String? {
        get { return configValue(forKey: key) }
        set { setDefaultValue(newValue, forKey: key) }
    }

    public subscript(_ key: RemoteConfigParam<Int>) -> Int? {
        get { return configValue(forKey: key) }
        set { setDefaultValue(newValue, forKey: key) }
    }

    public subscript(_ key: RemoteConfigParam<Float>) -> Float? {
        get { return configValue(forKey: key) }
        set { setDefaultValue(newValue, forKey: key) }
    }

    public subscript(_ key: RemoteConfigParam<Double>) -> Double? {
        get { return configValue(forKey: key) }
        set { setDefaultValue(newValue, forKey: key) }
    }

    public subscript(_ key: RemoteConfigParam<Bool>) -> Bool {
        get { return configValue(forKey: key) }
        set { setDefaultValue(newValue, forKey: key) }
    }

    public subscript(_ key: RemoteConfigParam<Data>) -> Data {
        get { return configValue(forKey: key) }
        set { setDefaultValue(newValue, forKey: key) }
    }

    public subscript(_ key: RemoteConfigParam<URL>) -> URL? {
        get { return configValue(forKey: key) }
        set { setDefaultValue(newValue, forKey: key) }
    }

    public subscript(_ key: RemoteConfigParam<UIColor>) -> UIColor? {
        get { return configValue(forKey: key) }
        set { setDefaultValue(newValue, forKey: key) }
    }

    public subscript<T>(_ key: DecodableRemoteConfigParam<T>) -> T? {
        return (try? configValue(forKey: key))?.flatMap { $0 }
    }

    public subscript<T: Codable>(_ key: CodableRemoteConfigParam<T>) -> T? {
        get {
            return (try? configValue(forKey: key))?.flatMap { $0 }
        }
        set {
            try? setDefaultValue(newValue, forKey: key)
        }
    }

    public subscript(default key: RemoteConfigParam<String>) -> String? {
        return getDefaultValue(forKey: key)
    }

    public subscript(default key: RemoteConfigParam<Int>) -> Int? {
        return getDefaultValue(forKey: key)
    }

    public subscript(default key: RemoteConfigParam<Float>) -> Float? {
        return getDefaultValue(forKey: key)
    }

    public subscript(default key: RemoteConfigParam<Double>) -> Double? {
        return getDefaultValue(forKey: key)
    }

    public subscript(default key: RemoteConfigParam<Bool>) -> Bool? {
        return getDefaultValue(forKey: key)
    }

    public subscript(default key: RemoteConfigParam<Data>) -> Data? {
        return getDefaultValue(forKey: key)
    }

    public subscript(default key: RemoteConfigParam<URL>) -> URL? {
        return getDefaultValue(forKey: key._key).flatMap(URL.init(string:))
    }

    public subscript(default key: RemoteConfigParam<UIColor>) -> UIColor? {
        return getDefaultValue(forKey: key._key)?.hexColor
    }

    public subscript<T>(default key: DecodableRemoteConfigParam<T>) -> T? {
        let data: Data? = {
            switch key.dataType {
            case .rawData: return (defaults[key._key] as? NSData) as Data?
            case let .json(encoding): return getDefaultValue(forKey: key._key)?.data(using: encoding)
            }
        }()
        return data.flatMap { (try? key.decoder.decode(T.self, from: $0)) }
    }

    public subscript(default key: AnyRemoteConfigParam) -> Any? {
        return self.defaults[key._key]
    }
}

public extension RemoteConfigServices {
    public func setDefaultValue<T>(_ value: T?, forKey key: CodableRemoteConfigParam<T>) throws {
        guard let value = value else {
            self.defaults[key._key] = nil
            self.updateDefaults()
            return
        }

        let data = try key.encoder.encode(value)
        switch key.dataType {
        case .rawData:
            self.defaults[key._key] = data as NSData
        case let .json(encoding):
            guard let json = String(data: data, encoding: encoding) else { return }
            self.defaults[key._key] = json as NSString
        }
        self.updateDefaults()
    }

    public func setDefaultValue(_ value: String?, forKey key: RemoteConfigParam<String>) {
        self.setDefaultValue(value, forKey: key._key)
    }

    public func setDefaultValue(_ value: String?, forKey key: String) {
        self.defaults[key] = value.map { $0 as NSString }
        self.updateDefaults()
    }

    public func setDefaultValue(_ value: Int?, forKey key: RemoteConfigParam<Int>) {
        self.setDefaultValue(value, forKey: key._key)
    }

    public func setDefaultValue(_ value: Int?, forKey key: String) {
        self.defaults[key] = value.map { $0 as NSNumber }
        self.updateDefaults()
    }

    public func setDefaultValue(_ value: Double?, forKey key: RemoteConfigParam<Double>) {
        self.setDefaultValue(value, forKey: key._key)
    }

    public func setDefaultValue(_ value: Double?, forKey key: String) {
        self.defaults[key] = value.map { $0 as NSNumber }
        self.updateDefaults()
    }

    public func setDefaultValue(_ value: Float?, forKey key: RemoteConfigParam<Float>) {
        self.setDefaultValue(value, forKey: key._key)
    }

    public func setDefaultValue(_ value: Float?, forKey key: String) {
        self.defaults[key] = value.map { $0 as NSNumber }
        self.updateDefaults()
    }

    public func setDefaultValue(_ value: Bool?, forKey key: RemoteConfigParam<Bool>) {
        self.setDefaultValue(value, forKey: key._key)
    }

    public func setDefaultValue(_ value: Bool?, forKey key: String) {
        self.defaults[key] = value.map { $0 as NSNumber }
        self.updateDefaults()
    }

    public func setDefaultValue(_ value: Data?, forKey key: RemoteConfigParam<Data>) {
        self.defaults[key._key] = value.map { $0 as NSData }
        self.updateDefaults()
    }

    public func setDefaultValue(_ value: URL?, forKey key: RemoteConfigParam<URL>) {
        self.defaults[key._key] = value.map { $0.absoluteString as NSString }
        self.updateDefaults()
    }

    public func setDefaultValue(_ value: UIColor?, forKey key: RemoteConfigParam<UIColor>) {
        self.defaults[key._key] = value.map { $0.hexString as NSString }
        self.updateDefaults()
    }
}

public extension RemoteConfigServices {
    public func configValue<T>(forKey key: DecodableRemoteConfigParam<T>) throws -> T? {
        let data: Data? = {
            switch key.dataType {
            case .rawData: return RemoteConfig.remoteConfig()[key._key].dataValue
            case let .json(encoding): return RemoteConfig.remoteConfig()[key._key].stringValue?.data(using: encoding)
            }
        }()
        return try data.map { try key.decoder.decode(T.self, from: $0) }
    }

    public func configValue(forKey key: RemoteConfigParam<String>) -> String? {
        return self.configValue(forKey: key._key)
    }

    public func configValue(forKey key: String) -> String? {
        return RemoteConfig.remoteConfig()[key].stringValue
    }

    public func configValue(forKey key: RemoteConfigParam<Int>) -> Int? {
        return self.configValue(forKey: key._key)
    }

    public func configValue(forKey key: String) -> Int? {
        return RemoteConfig.remoteConfig()[key].numberValue?.intValue
    }

    public func configValue(forKey key: RemoteConfigParam<Double>) -> Double? {
        return self.configValue(forKey: key._key)
    }

    public func configValue(forKey key: String) -> Double? {
        return RemoteConfig.remoteConfig()[key].numberValue?.doubleValue
    }

    public func configValue(forKey key: RemoteConfigParam<Float>) -> Float? {
        return self.configValue(forKey: key._key)
    }

    public func configValue(forKey key: String) -> Float? {
        return RemoteConfig.remoteConfig()[key].numberValue?.floatValue
    }

    public func configValue(forKey key: RemoteConfigParam<Bool>) -> Bool {
        return self.configValue(forKey: key._key)
    }

    public func configValue(forKey key: String) -> Bool {
        return RemoteConfig.remoteConfig()[key].boolValue
    }

    public func configValue(forKey key: RemoteConfigParam<Data>) -> Data {
        return RemoteConfig.remoteConfig()[key._key].dataValue
    }

    public func configValue(forKey key: RemoteConfigParam<URL>) -> URL? {
        return RemoteConfig.remoteConfig()[key._key].stringValue.flatMap(URL.init(string:))
    }

    public func configValue(forKey key: RemoteConfigParam<UIColor>) -> UIColor? {
        return self.configValue(forKey: key._key)?.hexColor
    }

    public func configValue(forKey key: AnyRemoteConfigParam) -> RemoteConfigValue {
        return RemoteConfig.remoteConfig()[key._key]
    }
}

public extension RemoteConfigServices {
    public func getDefaultValue(forKey key: RemoteConfigParam<String>) -> String? {
        return self.getDefaultValue(forKey: key._key)
    }

    public func getDefaultValue(forKey key: String) -> String? {
        return (self.defaults[key] as? NSString) as String?
    }

    public func getDefaultValue(forKey key: RemoteConfigParam<NSNumber>) -> NSNumber? {
        return self.getDefaultValue(forKey: key._key)
    }

    public func getDefaultValue(forKey key: String) -> NSNumber? {
        return self.defaults[key] as? NSNumber
    }

    public func getDefaultValue(forKey key: RemoteConfigParam<Int>) -> Int? {
        return self.getDefaultValue(forKey: key._key)
    }

    public func getDefaultValue(forKey key: String) -> Int? {
        return self.getDefaultValue(forKey: key)?.intValue
    }

    public func getDefaultValue(forKey key: RemoteConfigParam<Float>) -> Float? {
        return self.getDefaultValue(forKey: key._key)
    }

    public func getDefaultValue(forKey key: String) -> Float? {
        return self.getDefaultValue(forKey: key)?.floatValue
    }

    public func getDefaultValue(forKey key: RemoteConfigParam<Double>) -> Double? {
        return self.getDefaultValue(forKey: key._key)
    }

    public func getDefaultValue(forKey key: String) -> Double? {
        return self.getDefaultValue(forKey: key)?.doubleValue
    }

    public func getDefaultValue(forKey key: RemoteConfigParam<Bool>) -> Bool? {
        return self.getDefaultValue(forKey: key._key)
    }

    public func getDefaultValue(forKey key: String) -> Bool? {
        return self.getDefaultValue(forKey: key)?.boolValue
    }

    public func getDefaultValue(forKey key: RemoteConfigParam<Data>) -> Data? {
        return self.getDefaultValue(forKey: key._key)
    }

    public func getDefaultValue(forKey key: String) -> Data? {
        return (self.defaults[key] as? NSData) as Data?
    }
}

public extension RemoteConfigServices {
    public func setDefaultValue<T: RawRepresentable>(_ value: T?,
        forKey key: RemoteConfigParam<T>
    ) where T.RawValue == Int {
        self.defaults[key._key] = value.map { $0.rawValue as NSNumber }
        self.updateDefaults()
    }

    public func setDefaultValue<T: RawRepresentable>(_ value: T?,
        forKey key: RemoteConfigParam<T>
    ) where T.RawValue == String {
        self.defaults[key._key] = value.map { $0.rawValue as NSString }
        self.updateDefaults()
    }

    public func configValue<T: RawRepresentable>(forKey key: RemoteConfigParam<T>) -> T? where T.RawValue == Int {
        return self.configValue(forKey: key._key).flatMap { T(rawValue: $0) }
    }

    public func configValue<T: RawRepresentable>(forKey key: RemoteConfigParam<T>) -> T? where T.RawValue == String {
        return self.configValue(forKey: key._key).flatMap { T(rawValue: $0) }
    }

    public func getDefaultValue<T: RawRepresentable>(forKey key: RemoteConfigParam<T>) -> T? where T.RawValue == Int {
        return self.getDefaultValue(forKey: key._key).flatMap { T(rawValue: $0) }
    }

    public func getDefaultValue<T: RawRepresentable>(
        forKey key: RemoteConfigParam<T>
    ) -> T? where T.RawValue == String {
        return self.getDefaultValue(forKey: key._key).flatMap { T(rawValue: $0) }
    }

    public subscript<T: RawRepresentable>(_ key: RemoteConfigParam<T>) -> T? where T.RawValue == Int {
        get { return self.configValue(forKey: key) }
        set { self.setDefaultValue(newValue, forKey: key) }
    }

    public subscript<T: RawRepresentable>(_ key: RemoteConfigParam<T>) -> T? where T.RawValue == String {
        get { return self.configValue(forKey: key) }
        set { self.setDefaultValue(newValue, forKey: key) }
    }
}
