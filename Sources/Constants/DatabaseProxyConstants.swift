//
//  DatabaseProxyConstants.swift
//
//  Created by Jorge Villalobos on 11/11/18.
//  Copyright © 2018 Jorge Villalobos. All rights reserved.
//

import CommonsAssets
import Foundation

public enum DatabaseProxyConstants: CustomStringConvertible {
    case configuration
    case error

    public enum DomainErrors: CustomStringConvertible {
        case database
        case storage
        case auth
        case remoteConfig
        case facebook

        public var description: String {
            switch self {
            case .storage:
                return "StorageError"
            case .database:
                return "DatabaseError"
            case .auth:
                return "AuthError"
            case .remoteConfig:
                return "RemoteConfigError"
            case .facebook:
                return "FacebookError"
            }
        }
    }

    public var description: String {
        switch self {
        case .configuration:
            return "DatabaseProxyConfiguration"
        case .error:
            return "DatabaseProxyErrors"
        }
    }
}
